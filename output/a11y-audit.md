
   # Code: **WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2**

   ## Severity: **error**

   ## MessGE: **The html element should have a lang or xml:lang attribute which describes the language of the document.**

   ## Elements:
   
   ```
   html
   ```

   
   # Code: **WCAG2AA.Principle3.Guideline3_2.3_2_2.H32.2**

   ## Severity: **error**

   ## MessGE: **This form does not contain a submit button, which creates issues for those who cannot submit the form using the keyboard. Submit buttons are INPUT elements with type attribute "submit" or "image", or BUTTON elements with type "submit" or omitted/invalid.**

   ## Elements:
   
   ```
   #form1
   ```

   
   # Code: **WCAG2AA.Principle4.Guideline4_1.4_1_2.H91.A.NoContent**

   ## Severity: **error**

   ## MessGE: **Anchor element found with a valid href attribute, but no link content has been supplied.**

   ## Elements:
   
   ```
   #mainNav > div > ul > li:nth-child(1) > a
   ```

   
   # Code: **WCAG2AA.Principle4.Guideline4_1.4_1_2.H91.A.NoContent**

   ## Severity: **error**

   ## MessGE: **Anchor element found with a valid href attribute, but no link content has been supplied.**

   ## Elements:
   
   ```
   #mainNav > div > ul > li:nth-child(2) > a
   ```

   
   # Code: **WCAG2AA.Principle4.Guideline4_1.4_1_2.H91.A.NoContent**

   ## Severity: **error**

   ## MessGE: **Anchor element found with a valid href attribute, but no link content has been supplied.**

   ## Elements:
   
   ```
   #mainNav > div > ul > li:nth-child(3) > a
   ```

   
   # Code: **WCAG2AA.Principle4.Guideline4_1.4_1_2.H91.A.NoContent**

   ## Severity: **error**

   ## MessGE: **Anchor element found with a valid href attribute, but no link content has been supplied.**

   ## Elements:
   
   ```
   #mainNav > div > ul > li:nth-child(4) > a
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 1.5:1. Recommendation: change background to #907300.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(5) > div:nth-child(1) > div > div > h3
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 1.5:1. Recommendation: change background to #907300.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(5) > div:nth-child(2) > div > div > div > div:nth-child(5) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 1.5:1. Recommendation: change background to #907300.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(5) > div:nth-child(2) > div > div > div > div:nth-child(6) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 1.5:1. Recommendation: change background to #907300.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(5) > div:nth-child(2) > div > div > div > div:nth-child(7) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 1.5:1. Recommendation: change background to #907300.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(5) > div:nth-child(2) > div > div > div > div:nth-child(8) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 3.74:1. Recommendation: change background to #e6250f.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(9) > div:nth-child(1) > div > div > h2
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 3.74:1. Recommendation: change background to #e6250f.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(9) > div:nth-child(2) > div > div > div > div:nth-child(5) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 3.74:1. Recommendation: change background to #e6250f.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(9) > div:nth-child(2) > div > div > div > div:nth-child(6) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 3.74:1. Recommendation: change background to #e6250f.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(9) > div:nth-child(2) > div > div > div > div:nth-child(7) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail**

   ## Severity: **error**

   ## MessGE: **This element has insufficient contrast at this conformance level. Expected a contrast ratio of at least 4.5:1, but text in this element has a contrast ratio of 3.74:1. Recommendation: change background to #e6250f.**

   ## Elements:
   
   ```
   #form1 > div:nth-child(9) > div:nth-child(2) > div > div > div > div:nth-child(8) > a > div > span
   ```

   
   # Code: **WCAG2AA.Principle2.Guideline2_4.2_4_1.H64.1**

   ## Severity: **error**

   ## MessGE: **Iframe element requires a non-empty title attribute that identifies the frame.**

   ## Elements:
   
   ```
   #aswift_0
   ```

   
   # Code: **WCAG2AA.Principle2.Guideline2_4.2_4_1.H64.1**

   ## Severity: **error**

   ## MessGE: **Iframe element requires a non-empty title attribute that identifies the frame.**

   ## Elements:
   
   ```
   #google_osd_static_frame_1672958769965
   ```

   