
   ## Page: /

   ### Resolution: mobile

   ### PageSpeed Score: 50

   ### Usability Score: 98

   
    > Enable compression
    Rule Impact: 0.60
    http://www.switchtv.ke/img/switch-tv-logo.svg
    http://downloads.mailchimp.com/css/signup-forms/popup/1.0/layout-2.css
    http://downloads.mailchimp.com/css/signup-forms/popup/1.0/banner.css
  ---
  
    > Leverage browser caching
    Rule Impact: 38.55
    http://downloads.mailchimp.com/js/signup-forms/popup/unique-methods/1.0/popup.js
    http://downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js
    http://www.switchtv.ke/css/fontawesome-all.css
    http://www.switchtv.ke/css/metropolis.css
    http://www.switchtv.ke/css/style.css
    http://www.switchtv.ke/dist/js/bootstrap.min.js
    http://www.switchtv.ke/dist/js/jquery.min.js
    http://www.switchtv.ke/dist/js/main.min.js
    http://www.switchtv.ke/dist/js/popper.min.js
    http://www.switchtv.ke/img/banner3.jpg
    http://www.switchtv.ke/img/banner5.jpg
    http://www.switchtv.ke/img/chatspotbanner.png
    http://www.switchtv.ke/img/shows/1-thumb.jpg
    http://www.switchtv.ke/img/shows/10-thumb.jpg
    http://www.switchtv.ke/img/shows/11-thumb.jpg
    http://www.switchtv.ke/img/shows/14-thumb.jpg
    http://www.switchtv.ke/img/shows/3-thumb.jpg
    http://www.switchtv.ke/img/shows/30-thumb.jpg
    http://www.switchtv.ke/img/shows/4-thumb.jpg
    http://www.switchtv.ke/img/shows/48-thumb.jpg
    http://www.switchtv.ke/img/shows/49-thumb.jpg
    http://www.switchtv.ke/img/switch-tv-logo.svg
    http://www.switchtv.ke/img/switch168.png
    http://www.switchtv.ke/img/switchboard.png
    http://www.switchtv.ke/js/customize-slick.js
    http://www.switchtv.ke/js/slick.js
    https://gallery.mailchimp.com/91e225f463e09be10e2d4f3bc/images/2f0d0925-ac36-45ce-881d-33845331d1c5.png
    https://www.googletagmanager.com/gtag/js?id=UA-121841985-2
    http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js
    http://img.youtube.com/vi/-nICq4t0BHU/mqdefault.jpg
    http://img.youtube.com/vi/7u0jJW1VOco/mqdefault.jpg
    http://img.youtube.com/vi/ABubnGg7W0c/mqdefault.jpg
    http://img.youtube.com/vi/AXlPKxSrBz4/mqdefault.jpg
    http://img.youtube.com/vi/BvCbPLzNnIY/mqdefault.jpg
    http://img.youtube.com/vi/G5bCuI0h2RU/mqdefault.jpg
    http://img.youtube.com/vi/MWaDQtPB310/mqdefault.jpg
    http://img.youtube.com/vi/N6e5Lyq1RHk/mqdefault.jpg
    http://img.youtube.com/vi/NvK1VSIfIsE/mqdefault.jpg
    http://img.youtube.com/vi/PlohuJzQdvk/mqdefault.jpg
    http://img.youtube.com/vi/Px3L_nnujt4/mqdefault.jpg
    http://img.youtube.com/vi/Q7tbU8jfPTY/mqdefault.jpg
    http://img.youtube.com/vi/RrrTV1Th4lI/mqdefault.jpg
    http://img.youtube.com/vi/Vr6KsY9PCHo/mqdefault.jpg
    http://img.youtube.com/vi/WRt-4JVEMA0/mqdefault.jpg
    http://img.youtube.com/vi/XafjgtY_Gx0/mqdefault.jpg
    http://img.youtube.com/vi/YAsbxtfqnQ4/mqdefault.jpg
    http://img.youtube.com/vi/au8CcAaLQ9w/mqdefault.jpg
    http://img.youtube.com/vi/ijpGdWakhfo/mqdefault.jpg
    http://img.youtube.com/vi/jR6UehIpQNw/mqdefault.jpg
    http://img.youtube.com/vi/jev1s5twMoY/mqdefault.jpg
    http://img.youtube.com/vi/kgDKY8S8Mu8/mqdefault.jpg
    http://img.youtube.com/vi/qE-fODw2ogw/mqdefault.jpg
    http://img.youtube.com/vi/wlGg9UROHpg/mqdefault.jpg
    http://img.youtube.com/vi/xM6nGaGn890/mqdefault.jpg
    http://img.youtube.com/vi/zY-Ur_Ndx88/mqdefault.jpg
    https://www.google-analytics.com/analytics.js
  ---
  
    > Reduce server response time
    Rule Impact: 4.09
    
  ---
  
    > Minify CSS
    Rule Impact: 1.69
    http://www.switchtv.ke/css/fontawesome-all.css
    http://www.switchtv.ke/css/metropolis.css
  ---
  
    > Minify JavaScript
    Rule Impact: 0.49
    http://www.switchtv.ke/js/slick.js
    https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js
    http://www.switchtv.ke/js/customize-slick.js
    https://chimpstatic.com/mcjs-connected/js/users/91e225f463e09be10e2d4f3bc/36bdbcc152e6f3e11d10ff3ae.js
  ---
  
    > Eliminate render-blocking JavaScript and CSS in above-the-fold content
    Rule Impact: 24.00
    http://www.switchtv.ke/css/fontawesome-all.css
    http://www.switchtv.ke/css/metropolis.css
    http://www.switchtv.ke/css/style.css
  ---
  
    > Optimize images
    Rule Impact: 41.48
    http://www.switchtv.ke/img/shows/1-thumb.jpg
    https://img.new.livestream.com/videos/000000000af00297/48e7ad5c-48f1-48aa-88bf-441b40845e79_640x360.jpg
    http://www.switchtv.ke/img/shows/30-thumb.jpg
    http://www.switchtv.ke/img/shows/10-thumb.jpg
    http://www.switchtv.ke/img/banner3.jpg
    http://www.switchtv.ke/img/banner5.jpg
    http://www.switchtv.ke/img/shows/49-thumb.jpg
    http://www.switchtv.ke/img/shows/11-thumb.jpg
    http://www.switchtv.ke/img/shows/48-thumb.jpg
    http://www.switchtv.ke/img/shows/14-thumb.jpg
    http://www.switchtv.ke/img/shows/4-thumb.jpg
  ---
  
    > Size tap targets appropriately
    Rule Impact: 1.20
    <a href="./" class="navbar-brand"></a>
    <a href="https://www.fa…/switchtvkenya" class="nav-link text-white"></a>
    <button type="button" class="navbar-toggler…apsed border-0"></button>
    <a href="#carouselExampleIndicators" class="carousel-control-prev">Previous</a>
    <button class="button bannerC…ubscribeButton">Subscribe</button>
  ---
  

   
  
   ## Page: /

   ### Resolution: desktop

   ### PageSpeed Score: 6

   ### Usability Score: n/d

   
    > Enable compression
    Rule Impact: 0.60
    http://www.switchtv.ke/img/switch-tv-logo.svg
    http://downloads.mailchimp.com/css/signup-forms/popup/1.0/layout-2.css
    http://downloads.mailchimp.com/css/signup-forms/popup/1.0/banner.css
  ---
  
    > Leverage browser caching
    Rule Impact: 25.70
    http://downloads.mailchimp.com/js/signup-forms/popup/unique-methods/1.0/popup.js
    http://downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js
    http://www.switchtv.ke/css/fontawesome-all.css
    http://www.switchtv.ke/css/metropolis.css
    http://www.switchtv.ke/css/style.css
    http://www.switchtv.ke/dist/js/bootstrap.min.js
    http://www.switchtv.ke/dist/js/jquery.min.js
    http://www.switchtv.ke/dist/js/main.min.js
    http://www.switchtv.ke/dist/js/popper.min.js
    http://www.switchtv.ke/img/banner3.jpg
    http://www.switchtv.ke/img/banner5.jpg
    http://www.switchtv.ke/img/chatspotbanner.png
    http://www.switchtv.ke/img/shows/1-thumb.jpg
    http://www.switchtv.ke/img/shows/10-thumb.jpg
    http://www.switchtv.ke/img/shows/11-thumb.jpg
    http://www.switchtv.ke/img/shows/14-thumb.jpg
    http://www.switchtv.ke/img/shows/3-thumb.jpg
    http://www.switchtv.ke/img/shows/30-thumb.jpg
    http://www.switchtv.ke/img/shows/4-thumb.jpg
    http://www.switchtv.ke/img/shows/48-thumb.jpg
    http://www.switchtv.ke/img/shows/49-thumb.jpg
    http://www.switchtv.ke/img/switch-tv-logo.svg
    http://www.switchtv.ke/img/switch168.png
    http://www.switchtv.ke/img/switchboard.png
    http://www.switchtv.ke/js/customize-slick.js
    http://www.switchtv.ke/js/slick.js
    https://gallery.mailchimp.com/91e225f463e09be10e2d4f3bc/images/2f0d0925-ac36-45ce-881d-33845331d1c5.png
    https://www.googletagmanager.com/gtag/js?id=UA-121841985-2
    http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js
    http://img.youtube.com/vi/-nICq4t0BHU/mqdefault.jpg
    http://img.youtube.com/vi/7u0jJW1VOco/mqdefault.jpg
    http://img.youtube.com/vi/ABubnGg7W0c/mqdefault.jpg
    http://img.youtube.com/vi/AXlPKxSrBz4/mqdefault.jpg
    http://img.youtube.com/vi/BvCbPLzNnIY/mqdefault.jpg
    http://img.youtube.com/vi/G5bCuI0h2RU/mqdefault.jpg
    http://img.youtube.com/vi/MWaDQtPB310/mqdefault.jpg
    http://img.youtube.com/vi/N6e5Lyq1RHk/mqdefault.jpg
    http://img.youtube.com/vi/NvK1VSIfIsE/mqdefault.jpg
    http://img.youtube.com/vi/PlohuJzQdvk/mqdefault.jpg
    http://img.youtube.com/vi/Px3L_nnujt4/mqdefault.jpg
    http://img.youtube.com/vi/Q7tbU8jfPTY/mqdefault.jpg
    http://img.youtube.com/vi/RrrTV1Th4lI/mqdefault.jpg
    http://img.youtube.com/vi/Vr6KsY9PCHo/mqdefault.jpg
    http://img.youtube.com/vi/WRt-4JVEMA0/mqdefault.jpg
    http://img.youtube.com/vi/XafjgtY_Gx0/mqdefault.jpg
    http://img.youtube.com/vi/YAsbxtfqnQ4/mqdefault.jpg
    http://img.youtube.com/vi/au8CcAaLQ9w/mqdefault.jpg
    http://img.youtube.com/vi/ijpGdWakhfo/mqdefault.jpg
    http://img.youtube.com/vi/jR6UehIpQNw/mqdefault.jpg
    http://img.youtube.com/vi/jev1s5twMoY/mqdefault.jpg
    http://img.youtube.com/vi/kgDKY8S8Mu8/mqdefault.jpg
    http://img.youtube.com/vi/qE-fODw2ogw/mqdefault.jpg
    http://img.youtube.com/vi/wlGg9UROHpg/mqdefault.jpg
    http://img.youtube.com/vi/xM6nGaGn890/mqdefault.jpg
    http://img.youtube.com/vi/zY-Ur_Ndx88/mqdefault.jpg
    https://www.google-analytics.com/analytics.js
  ---
  
    > Reduce server response time
    Rule Impact: 2.73
    
  ---
  
    > Minify CSS
    Rule Impact: 1.69
    http://www.switchtv.ke/css/fontawesome-all.css
    http://www.switchtv.ke/css/metropolis.css
  ---
  
    > Minify JavaScript
    Rule Impact: 0.49
    http://www.switchtv.ke/js/slick.js
    https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js
    http://www.switchtv.ke/js/customize-slick.js
    https://chimpstatic.com/mcjs-connected/js/users/91e225f463e09be10e2d4f3bc/36bdbcc152e6f3e11d10ff3ae.js
  ---
  
    > Eliminate render-blocking JavaScript and CSS in above-the-fold content
    Rule Impact: 6.00
    http://www.switchtv.ke/css/fontawesome-all.css
    http://www.switchtv.ke/css/metropolis.css
    http://www.switchtv.ke/css/style.css
  ---
  
    > Optimize images
    Rule Impact: 546.46
    https://img.new.livestream.com/videos/000000000aed4287/e1d3cad0-53bc-4864-9c94-9db848f3aead_640x360.png
    https://img.new.livestream.com/videos/000000000aed33b5/6458a16d-7a1f-4314-a8ed-1cf982bee45a_640x360.png
    https://img.new.livestream.com/videos/000000000ae7ee3a/7dbf8ea9-950c-4d96-8b9f-3fbdbede74d5_640x360.png
    https://img.new.livestream.com/videos/000000000aeff1f1/6194e156-dc87-495a-b456-4afafc952388_640x360.png
    https://img.new.livestream.com/videos/000000000aead0e4/b5d60043-9089-48b2-ae7e-5ac25a187fb9_640x360.png
    https://img.new.livestream.com/videos/000000000aeaccd1/7c8f60a7-3512-4054-bfea-b7fc39721164_640x360.png
    https://img.new.livestream.com/videos/000000000aead0e8/5d8db505-1f3a-4e1d-9eb8-980ab59f748a_640x360.png
    https://img.new.livestream.com/videos/000000000aef4262/2e6d19d4-1dd7-44e8-b500-fa2b77cbe050_640x360.png
    https://img.new.livestream.com/videos/000000000aea0508/de961a1d-3cab-426a-9f85-430194d39cde_640x375.png
    https://img.new.livestream.com/videos/000000000aed33b1/ef81f76d-9383-46f9-9fba-d21ed6639fc5_640x360.png
    http://www.switchtv.ke/img/shows/30-thumb.jpg
    https://img.new.livestream.com/videos/000000000af00108/82df9137-25f1-4c9c-afff-21c62111e798_640x360.png
    https://img.new.livestream.com/videos/000000000af0dd56/0ed85489-cf58-4869-91b4-cb93401b3a17_640x360.png
    https://img.new.livestream.com/videos/000000000aef3997/bc2aa431-cff2-4827-95ed-181102df115c_640x360.png
    https://img.new.livestream.com/videos/000000000aeacefd/ff4cc5e9-0880-4b63-8fc6-3082283881d5_640x360.png
    https://img.new.livestream.com/videos/000000000aeb96ce/cdde973a-1a3c-43f2-bfca-75a89194cfb8_640x360.png
    http://www.switchtv.ke/img/shows/10-thumb.jpg
    http://www.switchtv.ke/img/shows/1-thumb.jpg
    https://img.new.livestream.com/videos/000000000af00297/48e7ad5c-48f1-48aa-88bf-441b40845e79_640x360.jpg
    http://www.switchtv.ke/img/shows/49-thumb.jpg
    http://www.switchtv.ke/img/shows/11-thumb.jpg
    http://www.switchtv.ke/img/shows/48-thumb.jpg
    http://www.switchtv.ke/img/shows/4-thumb.jpg
    http://www.switchtv.ke/img/shows/14-thumb.jpg
    http://www.switchtv.ke/img/banner3.jpg
    http://www.switchtv.ke/img/banner5.jpg
    https://img.new.livestream.com/videos/000000000af0c2e8/b63cd21a-a3e1-455c-9027-56ecbc94124c_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeac931/b46b3217-bb64-4986-a8a4-878c5786a5f3_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacdbc/f731d608-8550-45a5-bc32-f754218566e0_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeaccca/d5358af5-43e9-4c49-b732-92d7b9fe4b3f_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacdbe/bcec296a-88c5-4ed5-8bf1-3ce5ceb52983_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeac748/9a4a323f-e5c2-488f-b454-03de7d1ab257_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeac749/c190c89e-b691-4ed0-9f2a-49b89ed1496e_640x360.jpg
    https://img.new.livestream.com/videos/000000000ae7edda/13917984-d532-49d5-8d51-c71f7e0c62ad_640x360.jpg
    https://img.new.livestream.com/videos/000000000aec992d/9a812594-8ad7-4171-a1d9-aad1a5f45e8c_640x360.jpg
    https://img.new.livestream.com/videos/000000000aef2c05/604fb84f-0c90-419d-b1dd-af183cf9131f_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacdbd/5d50fb52-3d7b-4f9d-a202-32ac1b049dcc_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacb12/2aa4143f-14b2-45eb-87e0-51e4bfd68990_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacdb9/11a99942-c8ab-4b0d-84ed-ff919b4c2bd8_640x360.jpg
    https://img.new.livestream.com/videos/000000000ae7ed70/a5dd60ed-52fc-48fd-982b-247036eb03b9_640x360.jpg
    https://img.new.livestream.com/videos/000000000ae7ee78/6fa988b9-b08e-4d95-822b-a94826bd6f27_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacdba/4185d329-be15-4924-9273-84a9a2487f6a_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacccc/dd1d456f-0657-42f3-99f1-a8dbf4367a96_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeac643/adeab9ab-9000-41f6-8bcd-0a14d8d93121_640x360.jpg
    https://img.new.livestream.com/videos/000000000af0c7e8/46620509-fb0e-4f0c-9959-2f7811f318b8_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeac742/cb2059df-ef0b-4fc1-ba29-2e38df47fd20_640x360.jpg
    https://img.new.livestream.com/videos/000000000ae7edfb/74da1c69-b30f-4887-9617-82f0ce56627d_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeacdc0/799f05f1-ff21-4eff-98dc-efed09d5de11_640x360.jpg
    https://img.new.livestream.com/videos/000000000aed4b31/aaf40cfc-5a29-41fb-acd9-1c5eedf0955a_640x360.jpg
    https://img.new.livestream.com/videos/000000000af0e487/6b8ee0b6-0a72-4fa1-a373-8972eb5c85b0_640x360.jpg
    https://img.new.livestream.com/videos/000000000af0c2e9/48a2057c-104c-421a-955a-466bd237789d_640x360.jpg
    https://img.new.livestream.com/videos/000000000aec7736/2ae5bb5f-2457-4bf2-be3c-d93b17cd5c00_640x360.jpg
    https://img.new.livestream.com/videos/000000000aeb96cf/7a9c54c8-a02c-4bbc-9170-16ab8464ea8d_640x360.jpg
    https://img.new.livestream.com/videos/000000000af0c2e7/d7031566-3800-453a-b7b9-33e5781c4fe2_640x360.jpg
    https://img.new.livestream.com/videos/000000000aec732c/6d52e037-cd11-43b8-8f3a-97ac9adbfe17_640x360.jpg
  ---
  

   
  