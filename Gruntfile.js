module.exports = function(grunt) {
  const pngquant = require('imagemin-pngquant');
  const mozjpeg = require('imagemin-mozjpeg');
  // const webp = require('imagemin-webp');
  const svgo = require('imagemin-svgo');
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
        files: ['scss/*','node_modules/bootstrap/scss/**/*', 'js/*.js', 'img/*.jpg, *.png'],
        tasks: ['sass', 'uglify','postcss', 'cssmin']
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build:{
        files: [{
          expand: true,
          cwd: '.',
          src: ['js/*.js', '!js/*.min.js'],
          dest: 'dist',
          ext: '.min.js'
        }]
      }
      
    },
    sass: { 
      dist: {
        options: {
          style: 'expanded',
          update: true
        },
        files: [{
          expand: true,
          cwd: 'scss',
          src: ['*.scss'],
          dest: 'css/',
          ext: '.css'
        }]
      }
    },
    postcss: {
      options:{
        map:true,
        processors: [
        require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
      ]
      },
      dist:{
        src:'css/*.css'
      }
    },
    cssmin: {
      options:{
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: [{
          'dist/css/style.min.css': ['css/*.css', '!css/*.min.css']
          // expand: true,
          // cwd: '.',
          // src: ['css/*.css', '!css/*.min.css'],
          // dest: 'dist',
          // ext: '.min.css'
        }]
      }
    },
    imagemin: {
      options : {
        use: [
          pngquant({quality: '75'}),
          mozjpeg({quality: '75'}),
          // webp({quality:'75'}),
        ],
        svgo: [{removeViewBox: false}],
      },
      dynamic: {
        files: [{
          expand:true,
          cwd: 'img2/',
          src: ['**/*.{png,jpg,svg}'],
          dest:'img',
          // ext: ".webp"
        }]
      }
    },   
    browserSync: {
        dev: {
            bsFiles: {
                src: ['css/*.css', 'dist/css/*.min', '*.html', '*.php', 'includes/*.html','includes/*.php','js/*.js','img/*.jpg', 'img/*.png']
            },
            options: {
                watchTask: true,
                proxy:"localhost/scorepesa",
            }
        }
    }
   
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  // Default task(s).
  grunt.registerTask('default', [ 'browserSync', 'watch' ]);
  grunt.registerTask(['uglify','sass', 'postcss', 'cssmin', 'imagemin']);

};
